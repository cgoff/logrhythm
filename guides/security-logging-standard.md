# Information Security Logging Standard
## 1.	Overview
Logging from critical systems, applications and services can provide key information and potential indicators of compromise.  Although logging information may not be viewed daily, it is critical to have for incident response and forensics.
## 2.	Purpose
The purpose of this document attempts to address this issue by identifying specific requirements that information systems must meet to generate appropriate audit logs and integrate with an enterprise’s log management function.
## 3.  Scope
This standard applies to all production, critical test, and vendor systems on the `<business name>` network and SaaS applications utilized by `<business name>` (e.g. Microsoft Office 365).
## 4.	Standard
### 4.1 General Requirements
All systems that handle confidential information, accept network connections, or make access control (authentication and authorization) decisions shall record and retain audit-logging information sufficient to answer the following questions:
1.	What activity was performed?
2.	Who or what performed the activity, including where or on what system the activity was performed from (subject)?
3.	What the activity was performed on (object)?
4.	When was the activity performed?
5.	What tool(s) was the activity was performed with?
6.	What was the status (such as success vs. failure), outcome, or result of the activity?
### 4.2 Activities to be Logged
Logs must be created whenever any of the following activities are requested to be performed by the system:
1.	Create, read, update, or delete confidential information, including confidential authentication information such as passwords;
2.	Create, update, or delete information not covered in #1;
3.	Initiate a network connection;
4.	Accept a network connection;
5.	User authentication and authorization for activities covered in #1 or #2 such as user login and logout;
6.	Grant, modify, or revoke access rights, including adding a new user or group, changing user privilege levels, changing file permissions, changing database object permissions, changing firewall rules, and user password changes;
7.	System, network, or services configuration changes, including installation of software patches and updates, or other installed software changes;
8.	Application process startup, shutdown, or restart;
9.	Application process abort, failure, or abnormal end, especially due to resource exhaustion or reaching a resource limit or threshold (such as for CPU, memory, network connections, network bandwidth, disk space, or other resources), the failure of network services such as DHCP or DNS, or hardware fault;
10.	Detection of suspicious/malicious activity such as from an Intrusion Detection or Prevention System (IDS/IPS), or anti-malware system; and
11.	Detection of malicious shell or console commands.

### 4.3 Elements of the Log
Such logs must identify or contain at least the following elements, directly or indirectly. In this context, the term “indirectly” means unambiguously inferred.
1.	Type of action – examples include authorize, create, read, update, delete, and accept network connection.
2.	Subsystem performing the action – examples include process or transaction name, process or transaction identifier.
3.	Identifiers (as many as available) for the subject requesting the action – examples include user name, computer name, IP address, and MAC address. Note that such identifiers should be standardized in order to facilitate log correlation.
4.	Identifiers (as many as available) for the object the action was performed on – examples include file names accessed, unique identifiers of records accessed in a database, query parameters used to determine records accessed in a database, computer name, IP address, and MAC address. Note that such identifiers should be standardized in order to facilitate log correlation.
5.	Before and after values when action involves updating a data element, if feasible.
6.	Date and time the action was performed, including relevant time-zone information if not in Coordinated Universal Time.
7.	Whether the action was allowed or denied by access-control mechanisms.
8.	Description or reason-codes of why the action was denied by the access-control mechanism, if applicable.

### 4.4 Formatting and Storage
The system shall support the formatting and storage of audit logs in such a way as to ensure the integrity of the logs and to support enterprise-level analysis and reporting (SIEM).
#### 4.4.1 Log Formatting
1.	Microsoft Windows Event Logs collected by a Windows Event Forwarding;
2.	Logs in a well-documented standardized formats such as CEF, LEEF, or JSON sent via syslog (or similar standard protocol) to a centralized log management system;
3.	Logs stored in an SQL database that itself generates audit logs in compliance with the requirements of this document; and
4.	Other logging mechanisms supporting the above requirements such as those based on CheckPoint OPSEC.
5.	Dates and times will be normalized at the SIEM wherever possible.

#### 4.4.2 Log Retention
-	Minimum log file size to accommodate (`5`) days of activity on the local system.
-	`<business name>` will store logs a minimum of (`1`) year unless otherwise required by regulations or policy.
## 5	Onboarding Log Sources
-	Onboarding of Log Sources will utilize an established request process or catalog item in the IT service management platform.
## 6	Log Source Configuration
-	Log sources must alarm or alert on failure
-	Compliance-specific log sources must be associated with identifiers to ease categorization, reporting, and investigation.
-	All systems generating logs will utilize a minimum of (`2`) time servers (NTP) where supported.
-	Logs containing PHI will have the PHI masked prior to storage on the SIEM.
-	Logs will have file integrity checking enabled to ensure logs have not been tampered with.
-	Systems will enable X-FORWARDED-FOR or similar configuration, where supported, to preserve source and destination information through proxy services.
-	Procedures must exist for managing unusual events; response must be commensurate with system criticality, data sensitivity, and regulatory requirements.
-	Windows systems must be subscribed to Windows Event Forwarding.
-	An dedicated service account will be utilized for database monitoring.
## 7	Logging Custom Applications
-	Custom applications will generate logs with information defined in "Elements of a Log" (4.3).
## 8	Roles and Responsibilities
System Administrator
-	Configuration of local system to send supported logs to the SIEM.
-	Proper configuration of local system log generation to support the Information Security Logging Standard.

SIEM Administrator
-	Admitting new log sources to SIEM.
-	Managing the SIEM and supporting infrastructure per `<business name>` policy.
-	Responsible for proving reports and information to departments (e.g. HR or Audit) on request.
-	Management of the Information Security Logging Standard.

SIEM Analyst
-	Monitoring dashboards and alerts/alarms generated by the SIEM.
-	Performing authorized procedures as dictated by Security Team.
## 9	Related Standards, Policies and Processes
NIST 800-92

## 10	Definitions and Terms
SIEM—Security Information and Event Manager

SaaS—Software as a Service

CEF—Common Event Format

LEEF—Log Event Extended Format

PHI—Protected Health Information

## 11	Revision History
Date of Change 	Responsible	Summary of Change

`MM/DD/YY` `<name>`	`<comment>`


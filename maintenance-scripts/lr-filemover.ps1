<#
 #.Description
 # LogRhythm - Move files from one directory to another when the target directory is empty. Used to move spooled event or log files.
 # Created Chris Harding 26 June 12
 # Update By Micah Shelton on 4/6/2016 for Unprocessed Archives, using a less than count.
 # Update By Chris Goff on 2/6/2019 for cleaner code, fixed bug for incomplete processing, can handle any src/dst directory
#>

param (
    [Parameter(Mandatory=$true)][string]$Source,
    [Parameter(Mandatory=$true)][string]$Destination,
    [Parameter(Mandatory=$true)][string]$FileLimit,
    [Parameter(Mandatory=$true)][string]$SleepTime
)

$Now = Get-Date
$originationInfo = Get-ChildItem $Source | Measure-Object
Write-Host $Now, "Files in HOLD:", $originationInfo.count -foregroundcolor "red"

while ('$originationInfo.count -gt 0')
{

$Now = Get-Date
$destinationInfo = Get-ChildItem $Destination | Measure-Object
Write-Host $Now, "Files in process:" $destinationInfo.count -foregroundcolor "yellow"
If ($destinationInfo.count -lt $FileLimit)
  {
    $DestCount = Get-ChildItem $Source | Measure-Object
    if ($DestCount.count -lt 20)
    {
        Write-Host "***File Move Complete***" -foregroundcolor "white"
        exit
    }

    Write-Host $Now, "Moving" $FileLimit "for processing." -foregroundcolor "green"

    #Destination for files
    $PickupDirectory = Get-ChildItem -Path $Source

        $Counter = 0
        foreach ($file in $PickupDirectory) {
        if ($Counter -ne $FileLimit)
        {
            Write-Host $file.FullName -foregroundcolor "green" #Output file fullname to screen
            Move-Item $file.FullName -destination $Destination
            $Counter++
            }
        }
    $originationInfo = Get-ChildItem $Source | Measure-Object
    Write-Host $Now, "Files in HOLD:", $originationInfo.count -foregroundcolor "red"
  }
     Start-Sleep -s $SleepTime
}
Exit
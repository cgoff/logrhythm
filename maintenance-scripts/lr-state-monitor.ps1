<#
Copyright 2018 Chris Goff (cgoff@fastmail.fm)

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
OR PERFORMANCE OF THIS SOFTWARE.
#>

<# Some "Feeder" functionality code borrowed from LogRhythm "file mover" by Chris Harding and Mical Shelton #>

<#######################################>
<# lr-state-monitor.ps1 - Version 0.4  #>
<# https://bitbucket.org/c-g/logrhythm #>
<#######################################>

# What this does:
# Monitor number of files in state, if we see 25 or more kick over the Mediator,
# rename the folder, restart the Mediator, then feed the overflow files back in.

<######################>
<#   Configuration    #>
<######################>
$DebugPreference = "Continue" # Set to "SilentlyContinue" to remove debug messages
$state_dir = 'C:\Program Files\LogRhythm\LogRhythm Mediator Server\state' # Set to anything for debugging
$num_files = 25 # Default 25
$sleep_time = 900 # Default 900 (15 minutes)
$service_sleep = 120 # Default 120
$Mediator = "LogRhythm Mediator Server Service" # Default 'LogRhythm Mediator Server Service', can change to non-critical service for debugging
$StateFolders = "$state_dir\UnprocessedLogs", "$state_dir\UnprocessedArchives"
$FeederFileLimit = 5 # Default 5
$FeederSleepTime = 15 # Default 15

# Main monitor state loop
do {

# Get-Service-Status function to return the status of a service
function Get-Service-Status {
    $MediatorService = Get-Service -Name $Mediator
    $MediatorService.status
    return
}

#Main scan loop. This monitors the state folders until $num_files is exceeded, then start recovery
$b = $true
while ($b -eq $true) {
    $UnprocessedLogsCount = ( Get-ChildItem $StateFolders[0] | Measure-Object ).Count;
    $UnprocessedArchivesCount = (Get-ChildItem $StateFolders[1] | Measure-Object ).Count;
    if ($UnprocessedLogsCount -gt $num_files) {
        #Write-Debug "Do stuff on Unprocessed Logs."
        $WorkingDirectory = $StateFolders[0]
        break
    } elseif ($UnprocessedArchivesCount -gt $num_files) {
        #Write-Debug "Do stuff on Unprocessed Archives."
        $WorkingDirectory = $StateFolders[1]
        break
    } else {
        $seconds_to_minutes = $sleep_time / 60
        Write-Debug "Less than $num_files files detected in each state directory...sleeping for $seconds_to_minutes minutes."
        Write-Debug "$UnprocessedArchivesCount unprocessed archives currently exist."
        Write-Debug "$UnprocessedLogsCount unprocessed logs currently exist."
        Start-Sleep -Seconds $sleep_time
    }
}

Write-Warning "Problem detected. Taking corrective action."

# Stop the LogRhythm Mediator Service
$varTest = Get-Service-Status
if ($varTest -eq "Running") {
    while ($varTest -eq "Running") {
    stop-service $Mediator -force
    Write-Debug "Waiting for service to stop..."
    Start-Sleep -Seconds $service_sleep
    $varTest = Get-Service-Status
    }
} else {
    Write-Debug "Service Not Running. Continuing."
}

Write-Warning "Mediator service stopped."

# Rename $WorkingDirectory. When the mediator restarts it will re-create $WorkingDirectory.
#Write-Debug "Appending '.hold' to $WorkingDirectory"
$TempState = $WorkingDirectory + ".hold"
Rename-Item -Path $WorkingDirectory -NewName $TempState

# Check to make sure the rename went okay otherwise halt the script as there are problems.
$a = Test-Path $TempState
if (-not $a) {
    Write-Error "Error appending .hold to $WorkingDirectory. Halting script." -ErrorAction Stop
}

# Restart LogRhythm Mediator Service
$attempt_counter = 0
$varTest = Get-Service-Status
while ($varTest -eq "Stopped") {
    if ($attempt_counter -eq 7) {
        Write-Error "Unable to restart the Mediator service. Tried $attempt_counter times. Halting script." -ErrorAction Stop
    } else {
        $attempt_counter++
        Write-Debug "Starting Mediator Service...(Attempt #$attempt_counter)"
        Start-Service $Mediator
        Start-Sleep -seconds $service_sleep # Give it some time to start
        $varTest = Get-Service-Status
    }
}

# For debug purposes create the new $WorkingDirectory as if the mediator service started correctly.
New-Item -ItemType Directory -Force -Path $WorkingDirectory

# Feed files in the .hold directory to the fresh working directory
$FeederSource = "$WorkingDirectory.hold"
$FeederSourceCount = ( Get-ChildItem $FeederSource | Measure-Object ).Count;

while ($FeederSourceCount -gt 0)
{

$FeederDestinationCount = ( Get-ChildItem $WorkingDirectory | Measure-Object ).Count;
If ($FeederDestinationCount -lt $FeederFileLimit)
  {
    $DestCount = ( Get-ChildItem $FeederSource | Measure-Object ).Count;
    if ($DestCount -eq 0) {break}

    $PickupDirectory = Get-ChildItem -Path $FeederSource

        $Counter = 0
        foreach ($file in $PickupDirectory) {
        if ($Counter -ne $FeederFileLimit)
        {
            Move-Item $file.FullName -destination $WorkingDirectory
            $Counter++
            }
        }
    $FeederSourceCount = ( Get-ChildItem $FeederSource | Measure-Object ).Count;
  }
     Start-Sleep -s $FeederSleepTime
}

# Remove .hold temporary directory
Write-Host "Removing temporary hold directory."
Remove-Item "$WorkingDirectory.hold" -Force -Recurse

Write-Host "Repair complete. Continuing monitor loop."

# Sleep to allow service time to process left-over files before continuing loop
# This is to prevent any log processing lag from starting the recovery process loop again
Start-Sleep -Seconds $sleep_time
}
while (1 -eq 1)
import os
import time
import requests
import logging
from logging.handlers import SysLogHandler

requests.packages.urllib3.disable_warnings()

# Set environment variables for PPAPI_PRINCIPAL, and PPAPI_SECRET

# Service Principal; Must be configured from Proofpoint TAP settings
principal = os.environ["PPAPI_PRINCIPAL"]

# Secret; Must be configured from Proofpoint TAP setttings
secret = os.environ["PPAPI_SECRET"]

url = "https://tap-api-v2.proofpoint.com/v2/siem/" "all?format=syslog&sinceSeconds=60"

r = requests.get(url, verify=False, auth=(principal, secret))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("proofpoint-tap-output.log")
apiCallCount = 0


def queryProofpointTAP():
    while True:
        global logger
        global handler
        global apiCallCount

        os.system("cls" if os.name == "nt" else "clear")

        if logger.hasHandlers():
            logger.handlers.clear()

        logger.addHandler(handler)

        # Query the API
        r = requests.get(url, verify=False, auth=(principal, secret))
        apiCallCount += 1
        print("Last HTTP Response : {0}".format(r.status_code))
        print("Called API {0} times.".format(apiCallCount))

        if r.status_code != 204:
            # Write the output
            logging.info(r.text)

        # Sleep to prevent hitting the API rate-limiter
        # https://help.proofpoint.com/Threat_Insight_Dashboard/
        # API_Documentation/SIEM_API
        time.sleep(60)


def main():
    queryProofpointTAP()


if __name__ == "__main__":
    main()

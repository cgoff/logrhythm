#$DebugPreference = "Continue"

# Created by Chris Goff
# Test-ADAuthentication function from https://stackoverflow.com/a/7664380

# This is to generate Windows Event IDs 4776 and 4625 to test AIE engine rules

# Enter users to test against here
$users = ""

# Enter a generic or random password to use against the accounts
$p = ""

Function Test-ADAuthentication {
    param($username,$password)
    (new-object directoryservices.directoryentry "",$username,$password).psbase.name -ne $null
}

foreach ($user in $users) {
    $ADUser = Get-ADUser -Identity $user
    Test-ADAuthentication $ADUser.SamAccountName $p
}

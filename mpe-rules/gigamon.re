# Gigamon MPE Rules
# Tested LogRhythm 7.3.4

# 1. Management Authentication Activity
# Base-rule Regular Expression
Z\s((?<dname>.*?))(?<sessiontype>mgmtd).(<severity>)\s(<session>)\s-\s-\s[Uu]ser\s(<login>):\s(<action>)\sfrom\s<sip>

# Sub-rule "User Login"
# Sub-rule "User Logoff"

# 2. WSMD Authentication Activity (Web User Authentication Activity)
# Base-rule Regular Expression
Z\s(?<dname>.*?)\s(?<sessiontype>wsmd).(<severity>)\s<session>\s-\s-\s[Uu]ser\s(<login>)\s\((?<group>.*?)\)\s(?<action>.*?)from\s<sip>

# 3. CLI Activity
# Base-rule Regular Expression
Z\s(?<dname>.*?)\s(?<sessiontype>cli).(<severity>)\s<session>\s-\s-\s[Uu]ser\s<login>:\s(?<action>.*?)$

# Sub-rule "SSHd Authentication Failure"
# Sub-rule "SSHd Authentication Success"

# 4. Catch All
^<\d+>\s(\w+)\s(\d{2}\s\d{2}:\d{2}:\d{2})\s(<sname>)\s

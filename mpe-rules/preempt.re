# Preempt MPE riles for LogRhythm (tested 7.4.3)

# Incidents
^<\d{1,2}>\s<date>\s<hostname>\s:CEF:\d|<vendorid>|\w+|(?<ver>\d+.\d+.\d+)|<subject>|<msg>|.*?\sshost=<sname>\scs1=<url>$

# ifs for Incidents above--there are three possible values including shost in the logs
shost=<sname>
suser=<username>
src=<sip>

# System Notifications
^<\d+><date>\s\w\s:CEF:\d{1,3}|\w|\w|(?<ver>\d+.\d+.\d+)|<eventid>|<msg>|<severity>|.*?\sshost=<sname>\sdst=<dip>$

# System Audit
^<\d+><date>\s\w\s:CEF:\d+|\w|\w|(?<ver>\d+.\d+.\d+)|<eventid>|<msg>|\d+|.*?\ssuser=<suid>\ssrc=<sip>$


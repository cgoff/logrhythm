<#
Forces all interactive logon sessions to close
Usage: .\ForceLogoff.ps1 <hostname>

Quick PowerShell script to be used as a SmartResponse plug-in for LogRhythm
Created by Chris Goff (cgoff1@fairview.org)
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory=$true)][string]$hostname
)

Invoke-CimMethod -ClassName Win32_Operatingsystem -ComputerName $hostname -MethodName Win32Shutdown -Arguments @{ Flags = 4 }
'''
Copyright (c) 2019 Chris Goff <cgoff@fastmail.fm>

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
'''

# Grab Network information based on comment content.
# You'll need the API version; use a browser to visit https://<URL>/apidoc/
# of your Infoblox installation and look at the title.

import requests
from netaddr import *

requests.packages.urllib3.disable_warnings()

domain = ''
api_version = ''
api_username = ''
api_password = ''

r = requests.get('https://{0}/wapi/{1}/network?comment~:=guest', verify=False,
                 auth=('{2}', '{3}')).format(domain, api_version,
                                             api_username, api_password)

x = r.json()

for i in x:
    s1 = i['network']
    s2 = IPSet([s1])
    s3 = str(s2.iprange()).replace('-', '~')
    print(s3)

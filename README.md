# Collection of custom tooling for LogRhythm

*NOTE: I do not work for nor represent [LogRhythm](https://www.logrhythm.com)*. I'm just an engineer happy with their product. I am not reponsible for any issues caused by tooling or configurations herein to your LogRhythm system.

## api-integrations/get-infoblox-subnets.py
Python script which reaches out to an Infoblox API and extract subnet information. It is then converted to a format acceptable for use in LogRhythm Lists.

I recommend utilizing this as service or scheduled task, and piping the output to the list_import directory for Job Manager pickup.

Default location: `C:\Program Files\LogRhythm\LogRhythm Job Manager\config\list_import\`

The script will currently query any comments containing the word '*guest*'. This is easily modifiable by changing the query type to other methods per the [Infoblox API documentation](https://www.infoblox.com/wp-content/uploads/infoblox-deployment-infoblox-rest-api.pdf).

## collector-helpers/collector-proofpoint-siem-api.py
Python script which reaches out to the Proofpoint Threat Insight API and pulls logs in syslog format.

This will require you have a LogRhythm System Monitor available to consume the generated file (flat file settings under the System Monitor Configuration within the LogRhythm Console).

Place your Proofpoint API authorization information into environment variables (note comments in code) and run. Tested with Python `3.6`.

## guides/security-logging-standard.md
A concise template for an Information Security Logging Standard. A must have for any successful SIEM deployment.

## guides/SIEM-USE-CASE-DB.xlsx
SIEM Use Case development tool which automatically generates metrics from pivot tables; originally created by Ryan Voloch. I've extended some of the functionality and included test cases. Note this is an Excel workbook.

## maintenance-scripts/lr-archivist.ps1
PowerShell script which can

1. Delete files or directories older than date specified.
2. Move files or directories older than date specified.

The use cases involve automatically managing LogRhythm inactive archives. The `-Archive <months>` switch moves LogRhythm inactive archives off local disk to SAN (or similar cheap[er] bulk storage). The `-Clean <months>` switch is used to "trim" the archives on SAN after they are no longer required by policy.

I recommend creating a scheduled task with the script as the action when triggered.

### Syntax
`lr-archivist.ps1 -Source <directory> -Destination <directory> -Clean <months> -Archive <months>`

### Examples
Send any file or directory 3 months older than date task is run to archive:
`lr-archivist.ps1 -Source C:\Program Files\LogRhythm\Inactive Archives -Destination Z:\Archives -Archive 3`

Delete any file or directory from archive that is older than 1 year:
`lr-archivist.ps1 -Source Z:\Archives -Clean 12`

## maintenance-scripts/lr-filemover.ps1
PowerShell script that moves a pre-defined number of files from a source to a destination. This is based on an internal LogRhythm script which I've refactored to be more flexible and remove a bug where files were being left.

The use case for this is when the `processedlogs` directory fills on a LogRhythm Collector. Typically you stop the LogRhythm System Monitor Service, rename the `processedlogs` directory, restart the LogRhythm System Monitor Service, and hand-feed the now "orphaned" files back into the newly created `processedlogs` directory once the system stabilizes. This script removes the need to hand-feed what may be tens (or hundreds--don't let your Checkpoint firewall get away from you!) of thousands of files, and feed them in a manner that won't potentially overload the collector again.

I recommend mapping another collector's `processedlogs` directory to the problem system and using that as the destination. This allows you to make use of an under-utilized collector to get the logs into the mediator(s) while not risking overloading the original collector. Handy for troubleshooting while recovering (quickly).

## maintenance-scripts/lr-service-monitor.ps1
PowerShell script which monitors the state (`UnprocessedLogs` and `UnprocessedArchives`) of the Mediator it is executed on. Acts like a process (simply modify the variables under the CONFIGURATION section as necessary and execute). May require running under a privileged account.

When one or more of the state folders has more than X (default: 25) files contained, the script will stop the Mediator service, rename the existing state folder(s), restart the Mediator service (to resume processing), and it will feed the orphaned files back into the processor.

At this point I have chosen to not include `DXReliablePersist` monitoring. If this starts filling it is indicative of another issue. Changing the Mediator service status probably won't affect this issue in a meaningful way.

## mpe-rules/gigamon.re
Regular expressions for Gigamon events.
Make sure you get the sub-rules built out per the comments. This will capture all CLI, Web, and other user activity for auditing purposes.

## mpe-rules/protectwise.re
Regular expression for ProtectWise events.
Make sure you have your protectwse JSON emitter (`protectwise-emitter.json`) configured for SYSLOG output.

## mpe-rules/netiq_sspr.re
Collection of regular expressions for NetIQ's Single Sign-On Password Reset (SSPR) tool.

## smart-response-plugins/force-logoff
Smart Response Plug-in which forces all users to logoff. The use case for this is if X process fails, execute this plug-in on the kiosk which forces the process to restart.

## lr-threat-feeds.py [DEPRECATED]
The Poor Man's Threat Intelligence Feed Importer for LogRhythm. Note this is superceded by the Threat Intelligence module available from LogRhythm. Keeping for the memories or one off's where the aforementioned module isn't flexible enough.

Usage: `python lr-threat-feeds.py`

Dependencies: `netaddr` module


### Setup
The script is setup to run on the LogRhythm system containing the LogRhythm Job Manager. It can be run from anywhere that can talk to the LogRhythm Job Manager, just modify the path variable as needed.

If you do not have lists already created in LogRhythm, you will need to do so and specify the text file names in the list import.

Schedule `lr-threat-feeds.py` with Windows Task Scheduler.

### Feeds
- **firehol_level1**: http://iplists.firehol.org/
- **Abuse.ch Ransomware**: https://ransomwaretracker.abuse.ch/

Due to the way LogRhythm lists work, we cannot have a combined IP and IP range list. This script creates two LogRhythm list format compatible text files, one for single IPs, and another for IP ranges. Note that LogRhythm requires a tilde (~) to denote ranges (e.g 192.168.1.1~192.168.2.254).